package com.example.deadline;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.deadline.tables.User;
import com.example.deadline.tables.Task;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity {

    private BottomNavigationView bottomNavigationView;

    public static String USER_KEY = "USER_KEY";
    public static int REQUEST_CODE_GET_PHOTO = 101;

    LinearLayout layoutList;

    private CircleImageView mPhoto;
    private TextView mLogin;
    private TextView mPassword;

    private ProgressBar mprogressLab;
    private ProgressBar mprogressKur;
    private ProgressBar mprogressSt;

    User userInSession;

    private View.OnClickListener mOnPhotoClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            openGallery();
        }
    };

    private void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, REQUEST_CODE_GET_PHOTO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_CODE_GET_PHOTO
                && resultCode == Activity.RESULT_OK
                && data != null) {
            Uri photoUri = data.getData();
            mPhoto.setImageURI(photoUri);
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_profile);

        bottomNavigationView = (BottomNavigationView)findViewById(R.id.nav_menu);

        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.findItem(R.id.nav_profile);
        menuItem.setChecked(true);

        layoutList = findViewById(R.id.layout_list);

        mPhoto = findViewById(R.id.ivPhoto);
        mLogin = findViewById(R.id.tvEmail);

        mprogressLab = findViewById(R.id.progressBar);
        mprogressKur = findViewById(R.id.progressBar2);
        mprogressSt = findViewById(R.id.progressBar3);

        TextView labelLab = findViewById(R.id.textView23);
        TextView labelKur = findViewById(R.id.textView24);
        TextView labelSt = findViewById(R.id.textView25);

        labelLab.setText(String.format("%d%%", mprogressLab.getProgress()));
        labelKur.setText(String.format("%d%%", mprogressKur.getProgress()));
        labelSt.setText(String.format("%d%%", mprogressSt.getProgress()));

        Bundle bundle = getIntent().getExtras();
        userInSession = (User) bundle.getSerializable(USER_KEY);
        mLogin.setText(userInSession.getUserLogin());

        mPhoto.setOnClickListener(mOnPhotoClickListener);

//        setTasksForUser();
        showTasksGroupScroll();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.profile_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.actionLogout:
                startActivity(new Intent(this, AuthActivity.class));
                finish();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showTasksGroupScroll() {
        String[] strArray = new String[] {String.valueOf(userInSession.getId())};
        List<Task> taskGroupsCountByUser = Task.find(Task.class,"user_id = ?", strArray, "type", "id", null);

        for (Task task : taskGroupsCountByUser) {
            View tasksGroupScroll = getLayoutInflater().inflate(R.layout.tasks_group_scroll_add, layoutList, false);

            TextView textView1 = (TextView)tasksGroupScroll.findViewById(R.id.textViewTask1);
            TextView textView2 = (TextView)tasksGroupScroll.findViewById(R.id.textViewTask2);

            switch (task.getTaskType()) {
                case Task.TYPE_LAB:
                    int countLabs = Task.getTasksCountByTypeAndUser(Task.TYPE_LAB, userInSession);

                    tasksGroupScroll.setBackgroundResource(R.drawable.groups_profile_one);
                    textView1.setText("Лабы");
                    textView2.setText(countLabs + " Лаб");

                    break;
                case Task.TYPE_COURSE:
                    int countCourses = Task.getTasksCountByTypeAndUser(Task.TYPE_COURSE, userInSession);

                    tasksGroupScroll.setBackgroundResource(R.drawable.groups_profile_two);
                    textView1.setText("Курсы");
                    textView2.setText(countCourses + " Курсов");

                    break;
                case Task.TYPE_ARTICLE:
                    int countArticles = Task.getTasksCountByTypeAndUser(Task.TYPE_ARTICLE, userInSession);

                    tasksGroupScroll.setBackgroundResource(R.drawable.groups_profile_three);
                    textView1.setText("Статьи");
                    textView2.setText(countArticles + " Статей");

                    break;
            }

            layoutList.addView(tasksGroupScroll);
        }
    }

    public void setTasksForUser() {
        Task taskLab1 = new Task();
        taskLab1.setTaskUserId(userInSession.getId());
        taskLab1.setTaskName("Лаб 1");
        taskLab1.setTaskDescription("");
        taskLab1.setTaskType(Task.TYPE_LAB);
        taskLab1.setTaskTime(DateFormat.getDateFormat(getApplicationContext()));
        taskLab1.setTaskDone(false);
        taskLab1.save();

        Task taskLab2 = new Task();
        taskLab2.setTaskUserId(userInSession.getId());
        taskLab2.setTaskName("Лаб 2");
        taskLab2.setTaskDescription("");
        taskLab2.setTaskType(Task.TYPE_LAB);
        taskLab2.setTaskTime(DateFormat.getDateFormat(getApplicationContext()));
        taskLab2.setTaskDone(false);
        taskLab2.save();

        Task taskLab3 = new Task();
        taskLab3.setTaskUserId(userInSession.getId());
        taskLab3.setTaskName("Лаб 3");
        taskLab3.setTaskDescription("");
        taskLab3.setTaskType(Task.TYPE_LAB);
        taskLab3.setTaskTime(DateFormat.getDateFormat(getApplicationContext()));
        taskLab3.setTaskDone(false);
        taskLab3.save();

        Task taskArticle1 = new Task();
        taskArticle1.setTaskUserId(userInSession.getId());
        taskArticle1.setTaskName("Статья 1");
        taskArticle1.setTaskDescription("");
        taskArticle1.setTaskType(Task.TYPE_ARTICLE);
        taskArticle1.setTaskTime(DateFormat.getDateFormat(getApplicationContext()));
        taskArticle1.setTaskDone(false);
        taskArticle1.save();

        Task taskArticle2 = new Task();
        taskArticle2.setTaskUserId(userInSession.getId());
        taskArticle2.setTaskName("Статья 2");
        taskArticle2.setTaskDescription("");
        taskArticle2.setTaskType(Task.TYPE_ARTICLE);
        taskArticle2.setTaskTime(DateFormat.getDateFormat(getApplicationContext()));
        taskArticle2.setTaskDone(false);
        taskArticle2.save();
    }
}
