package com.example.deadline.tables;

import com.orm.SugarRecord;
import com.orm.dsl.Column;
import com.orm.dsl.Unique;

import org.apache.commons.codec.digest.DigestUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class User extends SugarRecord implements Serializable {
    public static final String USER_ID       = "id";
    public static final String USER_NAME     = "name";
    public static final String USER_LOGIN    = "login";
    public static final String USER_PASSWORD = "password";

    @Unique
//    @Column(name = USER_ID)
    private Long id;
    @Column(name = USER_NAME)
    private String userName;
    @Column(name = USER_LOGIN)
    private String userLogin;
    @Column(name = USER_PASSWORD)
    private String userPassword;

    public User() {
    }

    public User(String userLogin, String userPassword) {
        this.userName = userLogin.split("@")[0];
        this.userLogin = userLogin;
        this.userPassword = DigestUtils.md5Hex(userPassword);
    }

    public User(String userName, String userLogin, String userPassword) {
        this.userName = userName;
        this.userLogin = userLogin;
        this.userPassword = userPassword;
    }

    public Long getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + userLogin + '\'' +
                ", password='" + userPassword + '\'' +
                '}';
    }

    public static List<User> getUsers() {
        List<User> users = User.listAll(User.class);
        return users == null ? new ArrayList<User>() : users;
    }

    public static boolean addUser(User user) {
        List<User> users = getUsers();

        for (User u : users) {
            if (u.getUserLogin().equalsIgnoreCase(user.getUserLogin())) {
                return false;
            }
        }

        User newUser = new User();

        newUser.setUserName(user.getUserName());
        newUser.setUserLogin(user.getUserLogin());
        newUser.setUserPassword(user.getUserPassword());
        newUser.save();

        return true;
    }
}
