package com.example.deadline.tables;

import com.orm.SugarRecord;
import com.orm.dsl.Column;
import com.orm.dsl.Unique;

import java.io.Serializable;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;

public class Task extends SugarRecord implements Serializable {
    public static final String TASK_ID          = "id";
    public static final String TASK_USER_ID     = "user_id";
    public static final String TASK_NAME        = "name";
    public static final String TASK_DESCRIPTION = "description";
    public static final String TASK_TYPE        = "type";
    public static final String TASK_TIME        = "time";
    public static final String TASK_DONE        = "done";

    public static final String TYPE_LAB         = "lab";
    public static final String TYPE_COURSE      = "course";
    public static final String TYPE_ARTICLE     = "article";

    @Unique
//    @Column(name = TASK_ID)
    private Long id;
    @Column(name = TASK_USER_ID)
    private Long taskUserId;
    @Column(name = TASK_NAME)
    private String taskName;
    @Column(name = TASK_DESCRIPTION)
    private String taskDescription;
    @Column(name = TASK_TYPE)
    private String taskType;
    @Column(name = TASK_TIME)
    private DateFormat taskTime;
    @Column(name = TASK_DONE)
    private boolean taskDone;

    public Task() {
    }

    public Task(Long taskUserId, String taskName, String taskDescription, String taskType, DateFormat taskTime, boolean taskDone) {
        this.taskUserId = taskUserId;
        this.taskName = taskName;
        this.taskDescription = taskDescription;
        this.taskType = taskType;
        this.taskTime = taskTime;
        this.taskDone = taskDone;
    }

    public Long getId() {
        return id;
    }

    public Long getTaskUserId() {
        return taskUserId;
    }

    public void setTaskUserId(Long taskUserId) {
        this.taskUserId = taskUserId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    public String getTaskType() {
        return taskType;
    }

    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }

    public DateFormat getTaskTime() {
        return taskTime;
    }

    public void setTaskTime(DateFormat taskTime) {
        this.taskTime = taskTime;
    }

    public boolean isTaskDone() {
        return taskDone;
    }

    public void setTaskDone(boolean taskDone) {
        this.taskDone = taskDone;
    }

    public static List<Task> getTasksByUser(User user) {
        List<Task> tasks = Task.find(Task.class, "user_id = ?", String.valueOf(user.getId()));
        return tasks == null ? new ArrayList<Task>() : tasks;
    }

    public static int getTasksCountByTypeAndUser(String type, User user) {
        String[] values = {
                String.valueOf(user.getId()),
                type
        };

        return (int)Task.count(Task.class, "user_id = ? and type = ?", values);
    }
}
